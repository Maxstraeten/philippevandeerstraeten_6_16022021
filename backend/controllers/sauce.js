'use strict'; // Mode strict de ECMAScript 5
const Sauce = require('../models/Sauce');
const fs = require('fs');

// Obtenir l'ensemble des sauces
exports.getAllSauces = (req, res, next) => {
    Sauce.find()
    .then(sauces => res.status(200).json(sauces))
    .catch(error => res.status(400).json({ error: error }));
};

// Obtenir une sauce à partir de son id
exports.getOneSauce = (req, res, next) => {
    Sauce.findOne({ _id: req.params.id})
    .then(sauce => res.status(200).json(sauce))
    .catch(error => res.status(404).json({ error: error }));
}

// Créer la sauce 
exports.createSauce = (req, res, next) => {
    const sauceObject = JSON.parse(req.body.sauce);
    delete sauceObject._id; // suppression de l'id pour éviter les doublons avec mongoose
    const sauce = new Sauce({
        ...sauceObject,
        likes: 0,
        dislikes: 0,
        usersLiked: [],
        usersDisliked: [],
        imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}` //création du lien pour les images
    });
    console.log(sauce);
    sauce.save()
        .then(() => res.status(201).json({message: 'Création de la sauce réussie!'}))
        .catch((error) => res.status(400).json({error}));
};

// Modifier la sauce avec la possibilité de changer l'image
exports.modifySauce = (req, res, next) => {
    const sauceObject = req.file ?
    {
        ...JSON.parse(req.body.sauce),
        imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
    } : { ...req.body }; 
    Sauce.updateOne({ _id: req.params.id }, { ...sauceObject, _id: req.params.id })
        .then(() => res.status(200).json({ message: 'Sauce modifiée avec succès !'}))
        .catch(error => res.status(400).json({error}));
};

// Supprimer la sauce par le créateur de la sauce
exports.deleteSauce = (req, res, next) => {
    Sauce.findOne({ _id: req.params.id })
    .then(sauce =>{
        const filename = sauce.imageUrl.split('/images/')[1];
        fs.unlink(`images/${filename}`, () => {
            Sauce.deleteOne({ _id: req.params.id })
                .then(() => res.status(200).json({ message: 'Sauce Supprimée avec succès !'}))
                .catch(error => res.status(400).json({error}));
        });
    })
    .catch(error => res.status(500).json({error}));
};

// Ajouter des likes et des dislikes aux sauces
exports.likeSauce = (req, res, next) => {
    const sauceObject = req.body.sauce;
        Sauce.updateOne({ _id: req.params.id}, {set: {
            likes: sauceObject.likes,
            dislikes: sauceObject.dislikes,
            usersLiked: sauceObject.usersLiked,
            usersDisliked: sauceObject.usersDisliked },
            _id: req.params.id
        })
        sauceObject.save()
            .then(() => res.status(201).json({
                likes: sauceObject.likes,
                dislikes: sauceObject.dislikes,
                usersLiked: sauceObject.usersLiked,
                usersDisliked: sauceObject.usersDisliked 
            }))
            .catch(error => res.status(400).json({ error: req.body.message }));
};
