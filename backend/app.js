'use strict'; // Mode strict de ECMAScript 5
const express = require('express');
const helmet = require('helmet');
const morgan = require('morgan');
//const safe = require('safe-regex');
const mongoose = require('mongoose');
const rateLimiter = require('@markkuhn/express-rate-limiter');
const path = require('path');
const rfs = require('rotating-file-stream') 

const sauceRoutes = require('./routes/sauce');
const userRoutes = require('./routes/user');

const app = express();

// Configuration de Helmet
app.use(helmet());

// Configuration de Morgan et création des logs
const accessLogStream = rfs.createStream('access.log', {
    size: '10M',
    interval: '1d', 
    path: path.join(__dirname, 'log')
  });
app.use(morgan('combined', { stream: accessLogStream }));


// Lier la base de données MongoDb avec l'API
mongoose.connect('mongodb+srv://Philippe:SoPekocko59100@clustersopekocko.5ewr6.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',
{ useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true})
  .then(() => console.log('Connexion à MongoDB réussie !'))
  .catch(() => console.log('Connexion à MongoDB échoué !'));
  
  // Configuration de cors pour lier les deux serveurs
  app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
  });
  
// configuration de express-rateLimiter xxxxxxxxxxxxxxx
app.use(rateLimiter({
  requestsPerMinute: 100,
  identifier: function(req) {
      return req.connection.remoteAddress;
  },
  onBlocked: function(req, res) {
      res.sendStatus(429);
      console.log('Trop de requêtes');
  }
}));


// Body Parse modifié pour fonctionner avec express 4.17.1 xxxxxxxxxxx
app.use (express.urlencoded({extended: true}));
app.use (express.json()); 

// le chemin pour les images fixes
app.use('/images', express.static(path.join(__dirname, 'images')));

app.use('/api/sauces', sauceRoutes);
app.use('/api/auth', userRoutes);

// safe-regex
// let re;
// const ok = safe(re, opts={});
// console.log(ok);


module.exports = app;
