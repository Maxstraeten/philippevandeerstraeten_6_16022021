'use strict'; // Mode strict de ECMAScript 5
const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const sanitizerPlugin = require('mongoose-dompurify');

// Schema Mongoose pour les sauces
const sauceSchema = mongoose.Schema({
    userId: { type: String, trim: true, required: true},
    name: { type: String, trim: true, required: true, unique: true},
    manufacturer: { type: String, trim: true, required: true},
    description: { type: String, trim: true, required: true},
    mainPepper: { type: String, trim: true, required: true},
    imageUrl: { type: String, trim: true, required: true},
    heat: { type: Number, required: true},
    likes: { type: Number, required: true},
    dislikes: { type: Number, required: true},
    usersLiked: { type: [String], required: true },
    usersDisliked: { type: [String], required: true }
});

sauceSchema.plugin(uniqueValidator); // plugin mongoose-unique-validator
sauceSchema.plugin(sanitizerPlugin, { // plugin mongoose-dompurify 
    skip: [],
    encodeHtmlEntities: false,
    sanitizer: {
        ALLOWED_TAGS: [],
        FORBID_TAGS : [ 'svg' ] ,
        WHOLE_DOCUMENT : true ,
    }
  });

module.exports =  mongoose.model('Sauce', sauceSchema);
