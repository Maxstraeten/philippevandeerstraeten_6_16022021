'use strict'; // Mode strict de ECMAScript 5
const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const sanitizerPlugin = require('mongoose-dompurify');

// schema Mongoose pour les utilisateurs
const userSchema = mongoose.Schema({
    email: { type: String, trim: true, required: true, unique: true },
    password: { type: String, trim: true, required: true },
});

userSchema.plugin(uniqueValidator); // plugin mongoose-unique-validator
userSchema.plugin(sanitizerPlugin, { // plugin mongoose-dompurify
    skip: [],
    encodeHtmlEntities: false,
    sanitizer: {
        ALLOWED_TAGS: [],
        FORBID_TAGS : [ 'svg' ],
        WHOLE_DOCUMENT : true,
    }
  });

module.exports = mongoose.model('User', userSchema);
